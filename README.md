<div style="text-align:center">
    <img src="docs/logo-signatures.png" width="300" />
</div>

# Email Signature Generator

> Try it here: [https://signatures.nexioapp.com/](https://signatures.nexioapp.com/)

![img1](/.gitlab/img1.gif)

![img2](/.gitlab/img2.gif)

## 🔑 Key features

- Live preview
- Nice and editable HTML code
- High customization
- Custom templates (_New!_)
- Custom presets (_Coming soon!_)

## 📥 Installation

```
git clone git@gitlab.com:nexioinformatica/email-signature-generator.git
cd email-signature-generator
yarn start
```

## 🔧 Customize

**Add new signature template**

1. Add a `.mustache` template file in `public/template`

2. Add a signature preview (`.png` format) under `public/template` with same name of related template

3. Add your template to `public/template-list.json` specifying name, template uri and preview uri

```json
{
    "name": "Example",
    "uri": "/template/example.mustache",
    "preview": "/template/example.png"
},
```

4. Done

**Add new preset for user**

Fill out the following json object.

Note that

- only _legalName_, _email_, _address_, _firstName_ are required fields
- you need to specify a new _id_
- the group `guest` is visible to every user, otherwise the preset is shown only if the user logs in with the same word

```json
{
    "id": "1",
    "name": "Esempio",
    "group": "guest",
    "signature": {
        "firstName": "Mario",
        "lastName": "Rossi",
        "role": "Creatore di firme",
        "department": "Marketing",
        "site": "Pallino 2",
        "address": "Via Delle Industrie, 12, Pallino (25252), Italia",
        "phone": "+39 0364 123 456",
        "phoneInternal": "21",
        "email": "pinco.pallo@pallino.it",

        "legalName": "PALLINO SRL",
        "colorPrimary": "#000",
        "logoUrl": "https://signatures.nexioapp.com/signature/logo/logo.png",
        "websiteUrl": "https://www.pallino.it",
        "legalMapsUrl": "https://goo.gl/maps/ZwnFopVC5s5wXmxdA",
        "legalAddress": "Via Pinco Pallo, 42, Pallino (25252), Italia",
        "legalPhone": "+39 0364 123 456",
        "legalPhoneUrl": "tel:+39 0364 123 456",
        "legalEmail": "info@pallino.it",
        "legalEmailUrl": "mailto:info@pallino.it",

        "fbIconUrl": "https://signatures.nexioapp.com/signature/icons/fb.png",
        "fbUrl": "https://www.facebook.com/",
        "twitterIconUrl": "https://signatures.nexioapp.com/signature/icons/tt.png",
        "twitterUrl": "https://twitter.com/",
        "linkedinIconUrl": "https://signatures.nexioapp.com/signature/icons/ln.png",
        "linkedinUrl": "https://www.linkedin.com/",
        "youtubeIconUrl": "https://signatures.nexioapp.com/signature/icons/yt.png",
        "youtubeUrl": "https://www.youtube.com/",

        "phoneIconUrl": "https://signatures.nexioapp.com/signature/icons/phone-solid.svg",
        "emailIconUrl": "https://signatures.nexioapp.com/signature/icons/envelope-solid.svg"
    }
},
```

## 👤 Credits

**Luca Parolari**

- Github: [@lparolari](https://github.com/lparolari)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />
Feel free to check [issues page](https://gitlab.com/nexioinformatica/email-signature-generator/-/issues).
If you have any doubt or suggestion, please open an issue.

## 🦄 Show your support

Give a ⭐️ if this project helped or inspired you!

## 📝 License

Built with ❤️ by [Luca Parolari](https://github.com/lparolari).<br />
This project is [MIT](https://gitlab.com/nexioinformatica/email-signature-generator/-/blob/master/LICENSE) licensed.

**NEXIO INFORMATICA srl**

- Web: [nexioinformatica.com](https://www.nexioinformatica.com/)
- Gitlab: [@nexioinformatica](https://gitlab.com/nexioinformatica)

<img src="docs/logo-nexio.png" width="150" />
