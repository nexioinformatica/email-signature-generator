import { setupVersion } from "./version";

const bootstrap = () => {
  setupVersion();
};

export { bootstrap };
