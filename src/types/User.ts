export type User = {
  secret: string;
  presetGroup: string;
}

export const make = (secret: string) => (presetGroup: string): User => ({secret: secret, presetGroup: presetGroup});

export const makeBySecret = (secret: string) => make(secret)(secret);