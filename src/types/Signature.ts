interface UserInfo {
  firstName: string;
  lastName?: string;
}

interface UserRole {
  role?: string;
  department?: string;
  site?: string;
}

interface UserContacts {
  phone?: string;
  phoneInternal?: string;
  email: string;
  address: string;
}

interface Company {
  legalName: string;
  legalAddress?: string;
  legalMapsUrl?: string;
  legalPhone?: string;
  legalEmail?: string;
  websiteUrl?: string;
  logoUrl?: string;
}

interface CompanySocial {
  fbUrl?: string;
  twitterUrl?: string;
  linkedinUrl?: string;
  youtubeUrl?: string;
}

interface SignatureStyle {
  colorPrimary?: string;
  colorSecondary?: string;
  fbIconUrl?: string;
  twitterIconUrl?: string;
  linkedinIconUrl?: string;
  youtubeIconUrl?: string;
  phoneIconUrl?: string;
  emailIconUrl?: string;
}

export type Signature = UserInfo &
  UserRole &
  UserContacts &
  Company &
  CompanySocial &
  SignatureStyle;
