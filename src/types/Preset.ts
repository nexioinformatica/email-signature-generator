import { Signature } from "./Signature";

export type Preset = {
  id: string;
  group: string;
  name: string;
  signature: Signature;
};

export const toString = (preset: Preset): string => preset.name;