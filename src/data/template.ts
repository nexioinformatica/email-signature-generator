export const getDefaultTemplate = () => {
  return {
    uri: "/template/basic.mustache",
    name: "Predefinito",
    preview: "/template/basic.png",
  };
};
