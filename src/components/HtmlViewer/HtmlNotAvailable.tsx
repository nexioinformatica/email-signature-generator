import React from "react";
import { Typography } from "@material-ui/core";

export const HtmlNotAvailable = () => (
  <Typography>Html non disponibile.</Typography>
);
