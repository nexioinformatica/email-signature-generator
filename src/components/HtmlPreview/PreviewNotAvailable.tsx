import React from "react";
import { Typography } from "@material-ui/core";

export const PreviewNotAvailable = () => (
  <Typography>Preview non disponibile.</Typography>
);
