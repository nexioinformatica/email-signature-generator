#!/bin/bash

BUILD=build
TARGET_SERVER=nexioapp.com
NEW_RELEASE=$(date +%Y%m%d%H%M%S)
APP_DIR=/home/dyfsxpqn/dev/signatures-app
BUILD_DIR="$(pwd)/${BUILD}"
DEPLOY_DIR=${APP_DIR}/${BUILD}
NEW_RELEASE_DIR=${APP_DIR}/${NEW_RELEASE}
CURRENT_APP_SYMLINK=${APP_DIR}/current

function build {
    rm -r ${BUILD}
	yarn build
}

function pack {
	tar -cvf "${BUILD}.tar" ${BUILD}
}

function send {
	scp "${BUILD}.tar" ${TARGET_SERVER}:${APP_DIR}
}

function deploy {
    ssh ${TARGET_SERVER} "
        echo 'Releasing version ${NEW_RELEASE}';
        echo 'Extracting new files...';
        rm -rf ${DEPLOY_DIR};
        mkdir -p ${DEPLOY_DIR};
        tar -xvf '${APP_DIR}/${BUILD}.tar' -C ${APP_DIR};
        mv ${DEPLOY_DIR} ${NEW_RELEASE_DIR};
        echo 'Symlinking...';
        rm ${CURRENT_APP_SYMLINK};
        ln -s ${NEW_RELEASE_DIR} ${CURRENT_APP_SYMLINK};
        echo 'Done';
    "
}

function clear {
    rm -f ${BUILD}.tar
}

build
pack
send
deploy
clear
